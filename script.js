// 1. Цикли потрібні для того щоб атоматизувати монотонні однотипні дії та вказати умову при якій мета наших дій буде досягнута.
// 2. Для валідації введення коректних даних (коли ми не знаємо на перед скільки разів користувач помилиться) - краще цикл while.
//     Якщо потрібно здійснити відому на перед кількість ітерацій - цикл for. 
// 3. Явне перетворення відбувається при використанні методів, неявне - операторів.


 1.
 let userNum = prompt("enter your number.");

  if ((-5 < userNum && userNum < 5) === true) {console.log("Sorry, no numbers")
  } 
  
  for (i = 0; i <= userNum; i++) {
     
      if (i % 5 !== 0) continue;

      console.log(i);
  }

2.

"use strict"

const validator = number => {
    return parseInt(number)==number;
 
}

 const getNumber = () => {
     let numberOne = +prompt("Enter integer.");
     let numberTwo = +prompt("Enter integer.");
       while(!(validator(numberOne) && validator(numberTwo))){
        alert ("Error! Enter number again.");
        numberOne = +prompt("Enter integer.");
        numberTwo = +prompt("Enter integer.");
       };
      return [numberOne, numberTwo];
 }

const numbers = getNumber();


 let numberOne, numberTwo;
  
 if (numbers[0] > numbers[1]){
    numberOne = numbers[1];
    numberTwo = numbers[0];
 } else {
      numberTwo = numbers[1];
      numberOne = numbers[0];
 }

const division = number => {
    for (let i = 2; i < number; i++){
    
       if (!(number % i)) return false;
     }  return true;
 
}

for (let i=numberOne; i<numberTwo; i++) {
    if (division(i) === true) {
        console.log(i)
    }
}



